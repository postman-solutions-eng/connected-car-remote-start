![4Gr2iWk](https://user-images.githubusercontent.com/60015240/146800719-9ca46ac5-c2fd-4f14-bfca-baa3b557faa0.png)

# Connected Car Remote Start

Repository to store Automotive demo environment API versions from the Connected Car Workspace -> Remote Start API.
Our APIs that allow a consumer to connect directly to their car for convenience, security and improved experience.

**This workspace is part of a set of demo workspaces the Postman Solutions Engineering team put together to show how different industries can utilize workspaces to suit their needs, and does not represent a real company. You can read more about the process of creating this workspace in the companion [blog post](https://blog.postman.com/postman-enterprise-features-automotive-technology-industries/)**  

## Ownership

Collections and other assets within the corresponding workspace are managed by the [Connected-Car](https://automotive-demo.postman.co/team/group/82698/edit) group.

## Contributing

To propose an update to assets found in the corresponding workspace - please create a fork.

When ready, open a pull request and assign review to a member of the Connected-Car group.

## Overview of Collections

### Forks:
- Connect your Fleet: This is a fork from Mercedes-Benzes' collection for fleet management. Though this isn't something that's currently integrated into our system, it is a great model for how our future Fleet System could be written.
- Hazard Warnings: Another fork from the Mercedes-Benz workspace. Though our partnership with MB, we are able to tap into this location utility they have written and provide location-based hazard warnings on our in-car dashboards with appropriate attribution. (Integration currently in design phase.)

### PM Original Resources:
- Remote Start: mocked, monitored, contains tests
- Remote Unlock: mocked, monitored, contains tests
- Vehicle Status: mocked, monitored, contains tests
